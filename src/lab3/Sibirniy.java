package front;

import model.GameModel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;
import model.Cell;
import model.Mine;
import model.Wall;
import navigation.Coordinates;

/**
 *
 * @author grimb
 */
public class GameFieldPanel extends JPanel {
    // ------------------------------ Модель игры ------------------------------
    private GameModel _model;
    private GameModel _model_new; // First commit to my branch Sib
    private GameModel _model_new; // First commit to my branch Sib(Falstart)
    
    // ------------------------------ Размеры ---------------------------------
    
    private static final int CELL_SIZE = 30;
    private static final int GAP = 2;
    private static final int FONT_HEIGHT = 15;
    private static final int FONT_HEIGHT_NEW = 55; // Second commit to my branch Sib

    // ------------------------- Цветовое оформление ---------------------------
    
    private static final Color BACKGROUND_COLOR = new Color(175, 255, 175);
    private static final Color GRID_COLOR = Color.BLACK;
    private GridLayout layout = new GridLayout(0, 10);
}